## Top 100 Liked Questions (Easy & Medium)

### 1. Two Sum
https://leetcode.com/problems/two-sum/

### 9. Palindrome Number
https://leetcode.com/problems/palindrome-number/

### 13. Roman to Integer
https://leetcode.com/problems/roman-to-integer/

### 14. Longest Common Prefix
https://leetcode.com/problems/longest-common-prefix/

### 20. Valid Parentheses
https://leetcode.com/problems/valid-parentheses/

### 21. Merge Two Sorted Lists
https://leetcode.com/problems/merge-two-sorted-lists/

### 26. Remove Duplicates from Sorted Array
https://leetcode.com/problems/remove-duplicates-from-sorted-array/

### 27. Remove Element
https://leetcode.com/problems/remove-element/

### 35. Search Insert Position
https://leetcode.com/problems/search-insert-position/

### 66. Plus One
https://leetcode.com/problems/plus-one/


1. Two Sum

9. Palindrome Number

13. Roman to Integer

14. Longest Common Prefix

20. Valid Parentheses

21. Merge Two Sorted Lists

26. Remove Duplicates from Sorted Array

27. Remove Element

35. Search Insert Position

66. Plus One

69. Sqrt(x)

70. Climbing Stairs

88. Merge Sorted Array

94. Binary Tree Inorder Traversal

101. Symmetric Tree

104. Maximum Depth of Binary Tree

108. Convert Sorted Array to Binary Search Tree

118. Pascal's Triangle

121. Best Time to Buy and Sell Stock

125. Valid Palindrome

136. Single Number

141. Linked List Cycle

160. Intersection of Two Linked Lists

169. Majority Element

171. Excel Sheet Column Number

190. Reverse Bits

191. Number of 1 Bits

202. Happy Number

206. Reverse Linked List

217. Contains Duplicate

234. Palindrome Linked List

242. Valid Anagram

268. Missing Number

283. Move Zeroes

326. Power of Three

344. Reverse String

350. Intersection of Two Arrays II

387. First Unique Character in a String

412. Fizz Buzz









